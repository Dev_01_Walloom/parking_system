module Api::V1
  class LocationsController < BaseController
    def index
      @locations = Location.all
      render json: @locations
    end

    def new
      @location = Location.new
      render json: @location
    end

    def create
      @location = Location.new(location_params)
      if @location.save
        render json: @location
      else
        render json: @location.errors, status: :unprocessable_entity
      end
    end

    def edit
      @location = Location.find(params[:id])
      if @location
        render json: @location, each_serializer: LocationSerializer
      else
        render json: @location, status: :not_found
      end
    end

    def update
      @location = Location.find(params[:id])
      if @location.update_attributes(location_params)
        render json: @location
      else
        render json: @location.errors, status: :unprocessable_entity
      end
    end

    def destroy
      @location = Location.find(params[:id])
      if @location.destroy
        render json: {}, status: :ok
      else
        render json: @location.errors, status: :unprocessable_entity
      end
    end

    def location_params
      params.require(:location).permit(:id, :name, :latitude, :longitude, :address, :max_slots, :description)
    end
    private :location_params
  end
end
