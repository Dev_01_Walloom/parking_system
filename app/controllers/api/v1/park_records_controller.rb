module Api::V1
  class ParkRecordsController < BaseController
    before_action :vehicle, :slot, :location

    def index
      @park_record_presenter = ParkRecordPresenter.new(vehicles, slots, location)
      render json: @park_record_presenter.park_records_for_location, each_serializer: LocationParkRecordSerializer
    end

    def simulate
      @park_record = ParkRecord.new
    end

    def park
      park_record_presenter = ParkRecordPresenter.new(vehicles, slots, location)
      if park_record_presenter.create_park_records
        render json: park_record_presenter.created_records, each_serializer: LocationParkRecordSerializer
      else
        render json: "parking records could not be created", status: :unprocessable_entity
      end
    end

    def edit

    end

    def leave
      park_record_presenter = ParkRecordPresenter.new(vehicles, slots, location)
      if park_record_presenter.update_park_records
        render json: park_record_presenter.park_records_for_location, each_serializer: LocationParkRecordSerializer
      else
        render json: "parking records could not be updated", status: :unprocessable_entity
      end
    end

    def location
      @location = Location.find_by_id(params[:location_id])
    end
    private :location

    def vehicle
      @vehicle = Vehicle.find_by_id(params[:vehicle_id]) if params[:vehicle_id]
    end
    private :vehicle

    def slot
      @slot = Slot.find_by_id(params[:slot_id]) if params[:slot_id]
    end
    private :slot

    def slots
      @slots ||= Slot.by_location(location.id)
    end
    private :slots

    def vehicles
      @vehicles ||= Vehicle.all
    end
    private :vehicles

    def park_record_params
      params.require(:park_record).permit(:id,
                                          :vehicle_id,
                                          :slot_id,
                                          :entry_date,
                                          :exit_date,
                                          :comments,
                                          :total)
    end
    private :park_record_params
  end
end
