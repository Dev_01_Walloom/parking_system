class LocationSerializer < ActiveModel::Serializer
  attributes :id, :name, :latitude, :longitude, :address, :max_slots, :slots
  has_many :slots

  def slots
    object.slots
  end
  private :slots
end
