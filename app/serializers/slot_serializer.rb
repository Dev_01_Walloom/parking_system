class SlotSerializer < ActiveModel::Serializer
  attributes :id, :is_occupied, :location_id
end
