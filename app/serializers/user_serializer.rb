class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email, :birth_date, :is_admin, :vehicles
  has_many :vehicles

  def vehicles
    object.vehicles
  end
  private :vehicles
end
