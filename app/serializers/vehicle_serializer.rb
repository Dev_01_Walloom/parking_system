class VehicleSerializer < ActiveModel::Serializer
  attributes :id, :vin, :model, :year, :user

  def user
    object.user
  end
  private :user
end
